import React from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
// import { useHistory } from 'react-router-dom';
import { Link } from 'react-router-dom';
import productData from '../assets/fake-data/products';
import { useHistory } from 'react-router-dom';

class Checkout extends React.Component {
  constructor() {
    super();
    this.state = {
      total: 0,
      productInCarts: [],
    };
  }

  componentDidMount() {
    const userRegistered = JSON.parse(localStorage.getItem('users'));
    const foundUser = userRegistered.filter(
      user => user.email === localStorage.getItem('emailLogin')
    )[0];
    const myCart = foundUser.carts;
    const products = productData.getAllProducts();

    let productInCarts = [];
    let total = 0;
    products.forEach(product => {
      myCart.forEach(cart => {
        if (product.slug === cart.slug) {
          product.subTotal = parseInt(cart.quantity) * parseInt(product.price);
          product.quantity = parseInt(cart.quantity);
          total += product.subTotal;
          productInCarts.push(product);
        }
      });
    });
    this.setState({
      productInCarts: productInCarts,
      total: total,
    });
    console.log(productInCarts);
  }

  handleCheckout = () => {
    const userRegistered = JSON.parse(localStorage.getItem('users'));
    const foundUser = userRegistered.filter(
      user => user.email === localStorage.getItem('emailLogin')
    )[0];

    foundUser.carts = [];

    let userWithoutUserLogin = userRegistered.filter(
      user => user.email !== localStorage.getItem('emailLogin')
    );
    userWithoutUserLogin.push(foundUser);
    localStorage.setItem('users', JSON.stringify(userWithoutUserLogin));

    alert('Success to Checkout');
    this.props.history.push('/');
  };

  render() {
    return (
      <div>
        <h2>Checkout</h2>
        <hr />
        <div className='container'>
          <div className='row'>
            <div className='col-4 px-0'>
              <h3>Total : Rp {this.state.total} </h3>

              <div className='my-4'>
                <div className='mb-3'>
                  <label
                    for='exampleFormControlTextarea1'
                    class='h4 form-label'
                  >
                    Payment Method
                  </label>
                  <select
                    class='form-select h4'
                    aria-label='Default select example'
                  >
                    <option selected>Select the Payment Method</option>
                    <option value='1'>BCA Virtual Account</option>
                    <option value='2'>Gopay</option>
                    <option value='2'>Dana</option>
                  </select>
                </div>
                <div className='mb-3'>
                  <label
                    for='exampleFormControlTextarea1'
                    class='h4 form-label'
                  >
                    Delivery Method
                  </label>
                  <select
                    class='form-select h4'
                    aria-label='Default select example'
                  >
                    <option selected>Select the Delivery Method</option>
                    <option value='1'>JNE</option>
                    <option value='2'>COD</option>
                  </select>
                </div>
                <div class='mb-3'>
                  <label
                    for='exampleFormControlTextarea1'
                    class='h4 form-label'
                  >
                    Address
                  </label>
                  <textarea
                    class='form-control'
                    id='exampleFormControlTextarea1'
                    rows='3'
                  ></textarea>
                </div>
              </div>

              <div className='d-flex flex-column'>
                <button
                  onClick={this.handleCheckout}
                  className='btn btn-primary w-100 mb-3 py-3'
                >
                  Checkout
                </button>
              </div>
            </div>
            <div className='col-8 ps-5'>
              <h3>Products</h3>
              {this.state.productInCarts.map(product => {
                return (
                  <React.Fragment key={product.slug}>
                    <div className='d-flex align-items-center justify-content-between mb-3'>
                      <img
                        style={{ width: '120px' }}
                        src={product.image01}
                        alt=''
                      />
                      <div className='d-flex flex-column'>
                        <span className='h4'>{product.title}</span>
                        <span className='h3'>QTY : {product.quantity}</span>
                      </div>
                      <div className='d-flex flex-column'>
                        <span className='h6'>Sub Total :</span>
                        <span className='h3'>Rp {product.subTotal}</span>
                      </div>
                    </div>
                  </React.Fragment>
                );
              })}
            </div>
          </div>
        </div>
      </div>
    );
  }
}

const CheckoutNavigate = () => {
  const history = useHistory();
  return <Checkout history={history} />;
};

export default CheckoutNavigate;