import React from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import { Link } from "react-router-dom";
import { useHistory } from "react-router-dom";
import productData from "../assets/fake-data/products";

class MyCart extends React.Component {
  constructor() {
    super();
    this.state = {
        total: 0,
      productInCarts: [],
    };
  }

  componentDidMount() {
    const userRegisterd = JSON.parse(localStorage.getItem("users"));
    const foundUser = userRegisterd.filter(
      (user) => user.email === localStorage.getItem("emailLogin")
    )[0];
    const myCart = foundUser.carts;
    const products = productData.getAllProducts();

    let productInCarts = [];
    let total = 0
    products.forEach((product) => {
      myCart.forEach((cart) => {
        if (product.slug === cart.slug) {
          product.subTotal = parseInt(cart.quantity) * parseInt(product.price);
          product.quantity = parseInt(cart.quantity);
          total += product.subTotal;
          productInCarts.push(product);
        }
      });
    });
    this.setState({
      productInCarts: productInCarts,
      total: total,
    });
    console.log(productInCarts);
  }

  handleCheckout = () => {
    const userRegisterd = JSON.parse(localStorage.getItem("users"));
    const foundUser = userRegisterd.filter(
      (user) => user.email === localStorage.getItem("emailLogin")
    )[0];
    console.log(foundUser);
    // validasi jika cart kosong
    if (foundUser.carts.length === 0) {
      return alert("Please add product to Cart!");
    } else {
      //pergi checkout
      this.props.history.push("/checkout");
    }
  };

  render() {
    return (
      <div>
        <h2>MyCart</h2>
        <hr />
        <div className="container">
          <div className="row">
            <div className="col-4 px-0">
              <h3>Total :Rp {this.state.total}</h3>
              <div className="d-flex flex-column">
                <button
                  onClick={this.handleCheckout}
                  className="btn btn-primary w-100 mb-3 py-3"
                >
                  Checkout
                </button>
                <Link
                  to="/catalog"
                  className="btn btn-outline-primary w-100 py-3"
                >
                  Continue Shopping
                </Link>
              </div>
            </div>
            <div className="col-8 ps-5">
              <h3>Products</h3>
              {this.state.productInCarts.map((product) => {
                return (
                  <React.Fragment key={product.slug}>
                    <div className="d-flex align-items-center justify-content-between mb-3">
                      <img
                        style={{ width: "120px" }}
                        src={product.image01}
                        alt=""
                      />
                      <div className="d-flex flex-column">
                        <span className="h4">{product.title}</span>
                        <span className="h3">QTY : {product.quantity}</span>
                      </div>
                      <div className="d-flex flex-column">
                        <span className="h6">Sub Total :</span>
                        <span className="h3">Rp {product.subTotal}</span>
                      </div>
                    </div>
                  </React.Fragment>
                );
              })}
            </div>
          </div>
        </div>
      </div>
    );
  }
}

const MyCartNavigate = () => {
  const history = useHistory();
  return <MyCart history={history} />;
};

export default MyCartNavigate;
