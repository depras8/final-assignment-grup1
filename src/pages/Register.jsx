import React from "react";
import { Link, useHistory } from "react-router-dom";
import "bootstrap/dist/css/bootstrap.min.css";

class Register extends React.Component {
  constructor() {
    super();
    this.state = {
      name: "",
      email: "",
      password: "",
      address: "",
    };
  }

  componentDidMount() {
    document.title = "Register - Second Goods";
  }

  handleSubmit = (e) => {
    e.preventDefault();

    if (!this.state.name || this.state.name === "") {
      return alert("Please input your name!");
    }
    if (!this.state.name || this.state.email === "") {
      return alert("Please input your email!");
    }
    if (!this.state.name || this.state.password === "") {
      return alert("Please input your password!");
    }
    if (!this.state.name || this.state.address === "") {
      return alert("Please input your address!");
    }

    const newUser = {
      name: this.state.name,
      email: this.state.email,
      password: this.state.password,
      address: this.state.address,
      carts: [],
    };
    console.log(newUser);

    //simpan user baru ke local storage
    if (
      !JSON.parse(localStorage.getItem("users")) ||
      JSON.parse(localStorage.getItem("users")) === null
    ) {
      localStorage.setItem("users", JSON.stringify([]));
    }

    const users = JSON.parse(localStorage.getItem("users"));

    // validasi jika email sudah terdaftar
    let isEmailRegistered = null;
    users.forEach((user) => {
      if (user.email === this.state.email) {
        isEmailRegistered = true;
      }
    });

    if (isEmailRegistered) {
      return alert("email has been registered!");
    } else {
        users.push(newUser);
        localStorage.setItem("users", JSON.stringify(users));
        this.props.history.push("/login");
    }
  };

  render() {
    return (
      <div className="container">
        <div className="row">
          <div className="col-12 col-lg-6 mx-auto pt-5">
            <form onSubmit={(e) => this.handleSubmit(e)}>
              <h2 className="text-center">Register</h2>
              <hr className="mb-5" />
              <div className="mb-3">
                <label htmlFor="exampleInputEmail1" className="form-label">
                  Name
                </label>
                <input
                  type="text"
                  className="form-control py-3"
                  id="name"
                  aria-describedby="emailHelp"
                  onChange={(e) => this.setState({ name: e.target.value })}
                />
              </div>
              <div className="mb-3">
                <label htmlFor="exampleInputEmail1" className="form-label">
                  Email address
                </label>
                <input
                  type="email"
                  className="form-control py-3"
                  id="exampleInputEmail1"
                  aria-describedby="emailHelp"
                  onChange={(e) => this.setState({ email: e.target.value })}
                />
              </div>
              <div className="mb-3">
                <label htmlFor="exampleInputPassword1" className="form-label">
                  Password
                </label>
                <input
                  type="password"
                  className="form-control py-3"
                  id="exampleInputPassword1"
                  onChange={(e) => this.setState({ password: e.target.value })}
                />
              </div>
              <div className="mb-3">
                <label
                  htmlFor="exampleFormControlTextarea1"
                  className="form-label"
                >
                  Address
                </label>
                <textarea
                  className="form-control"
                  id="exampleFormControlTextarea1"
                  rows="3"
                  onChange={(e) => this.setState({ address: e.target.value })}
                ></textarea>
              </div>
              <div className="text-center my-4">
                Already have account?{" "}
                <Link className="text-primary" to="/login">
                  Login
                </Link>
              </div>
              <button type="submit" className="btn btn-primary w-100 py-3 mt-3">
                Register
              </button>
            </form>
          </div>
        </div>
      </div>
    );
  }
}

const RegisterNavigate = () => {
  const history = useHistory();
  return <Register history={history} />;
};

export default RegisterNavigate;
