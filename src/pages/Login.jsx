import React from "react";
import { Link } from "react-router-dom";
import "bootstrap/dist/css/bootstrap.min.css";
import { useHistory } from "react-router-dom/cjs/react-router-dom.min";

class Login extends React.Component {
  constructor() {
    super();
    this.state = {
      email: "",
      password: "",
    };
  }

  componentDidMount() {
    document.title = "Login - Second Goods";
    window.scroll(0, 0);
  }

  handleSubmit = (e) => {
    e.preventDefault();

    if (!this.state.email || this.state.email === "") {
      return alert("Please input your email!");
    }
    if (!this.state.password || this.state.password === "") {
      return alert("Please input your password!");
    }

    if (
      !JSON.parse(localStorage.getItem("users")) ||
      JSON.parse(localStorage.getItem("users")) === null
    ) {
      localStorage.setItem("users", JSON.stringify([]));
    }

    const users = JSON.parse(localStorage.getItem("users"));
    //cek email sudah terdaftar atau belum
    const foundUser = users.filter(
      (user) => user.email === this.state.email
    )[0];
    if (foundUser) {
      // jika terdaftar, cek password
      if (foundUser.password === this.state.password) {
        // jika berhasil arahkan ke home
        localStorage.setItem("isLogin", true);
        localStorage.setItem("emailLogin", foundUser.email);
        this.props.history.push("/");
        alert("Succes login...");
      } else {
        alert("email or password is wrong!");
      }
    } else {
      return alert("Email is not registered");
    }
  };

  render() {
    return (
      <div className="container">
        <div className="row">
          <div className="col-12 col-lg-6 mx-auto pt-5">
            <form onSubmit={(e) => this.handleSubmit(e)}>
              <h2 className="text-center">Login</h2>
              <hr className="mb-5" />
              <div className="mb-3">
                <label htmlFor="exampleInputEmail1" className="form-label">
                  Email address
                </label>
                <input
                  type="email"
                  className="form-control py-3"
                  id="exampleInputEmail1"
                  aria-describedby="emailHelp"
                  onChange={(e) => this.setState({ email: e.target.value })}
                />
              </div>
              <div className="mb-3">
                <label htmlFor="exampleInputPassword1" className="form-label">
                  Password
                </label>
                <input
                  type="password"
                  className="form-control py-3"
                  id="exampleInputPassword1"
                  onChange={(e) => this.setState({ password: e.target.value })}
                />
              </div>
              <div className="text-center my-4">
                Dont have account?{" "}
                <Link className="text-primary" to="/register">
                  Register
                </Link>
              </div>
              <button type="submit" className="btn btn-primary w-100 py-3 mt-3">
                Login
              </button>
            </form>
          </div>
        </div>
      </div>
    );
  }
}

const LoginNavigate = () => {
  const history = useHistory();
  return <Login history={history} />;
};

export default LoginNavigate;
