const img1 = require("../images/slider/slide_1.webp").default
const img2 = require("../images/slider/slide_2.jpg").default
const img3 = require("../images/slider/slide_3.jpg").default

const heroSliderData = [
    {
        title: "The Leading ReCommerce Platform in Indonesia",
        description: "We believe that ReCommerce can raise the standard of living of the Middle Class community at an affordable price.",
        img: img1,
        color: "blue",
        path: "/catalog/ao-thun-dinosaur-01"
    },
    {
        title: "Good Price Good Quality",
        description: "We select the quality of goods according to the price",
        img: img2,
        path: "/catalog/ao-thun-dinosaur-02",
        color: "pink"
    },
    {
        title: "Guarantee for 30 days",
        description: "We provide a 30 day warranty with terms and conditions apply",
        img: img3,
        path: "/catalog/ao-thun-dinosaur-03",
        color: "orange"
    }
]

export default heroSliderData