const product_01_image_01 = require('../images/products/product-01 (1).webp').default
const product_01_image_02 = require('../images/products/product-01 (2).webp').default
// const product_01_image_03 = require('../images/products/product-01 (3).webp').default

const product_02_image_01 = require('../images/products/product-02 (1).webp').default
const product_02_image_02 = require('../images/products/product-02 (2).webp').default

const product_03_image_01 = require('../images/products/product-03 (1).webp').default
const product_03_image_02 = require('../images/products/product-03 (2).webp').default

const product_04_image_01 = require('../images/products/product-04 (1).webp').default
const product_04_image_02 = require('../images/products/product-04 (2).webp').default

const product_05_image_01 = require('../images/products/product-05 (1).webp').default
const product_05_image_02 = require('../images/products/product-05 (2).webp').default

const product_06_image_01 = require('../images/products/product-06 (1).webp').default
const product_06_image_02 = require('../images/products/product-06 (2).webp').default

const product_07_image_01 = require('../images/products/product-07 (1).webp').default
const product_07_image_02 = require('../images/products/product-07 (2).webp').default

const product_08_image_01 = require('../images/products/product-08 (1).webp').default
const product_08_image_02 = require('../images/products/product-08 (2).webp').default

const product_09_image_01 = require('../images/products/product-09 (1).webp').default
const product_09_image_02 = require('../images/products/product-09 (2).webp').default

const product_10_image_01 = require('../images/products/product-10 (1).webp').default
const product_10_image_02 = require('../images/products/product-10 (2).webp').default

const product_11_image_01 = require('../images/products/product-11 (1).webp').default
const product_11_image_02 = require('../images/products/product-11 (2).webp').default

const product_12_image_01 = require('../images/products/product-12 (1).webp').default
const product_12_image_02 = require('../images/products/product-12 (2).webp').default

const products = [
    {
        title: "Huawei Y7 Pro",
        price: '559000',
        image01: product_01_image_01,
        image02: product_01_image_02,
        categorySlug: "others",
        colors: ["white", "black"],
        slug: "hp-no-01",
        storage: ["128"],
        description: "The Y7 Pro 2019 and the Y9 2019, launched last year, have an eerily similar design and structure given that they are both from the Y-series lineup. It has a glossy polycarbonate back panel with a gradient color pattern which still looks very stylish and premium. The bezels are slim on almost all sides except for the bottom since it houses the Huawei logo at its center. My opinion still hasn’t changed when it comes to these kinds of designs that mostly caters to the youth. It’s one of the ideal phones to get when you are out with friends or just hanging out casually, but I don’t think it’s appropriate to use it for corporate use or any other formal activities that you’ll attend to since it’s too much of an eye-catcher."
    },
    {
        title: "Iphone XS MAX 64GB",
        price: '2450000',
        image01: product_02_image_01,
        image02: product_02_image_02,
        categorySlug: "iphone",
        colors: ["white", "black"],
        slug: "hp-no-02",
        storage: ["64"],
        description: "The Iphone XS MAX Pro 2019 and the Y9 2019, launched last year, have an eerily similar design and structure given that they are both from the Y-series lineup. It has a glossy polycarbonate back panel with a gradient color pattern which still looks very stylish and premium. The bezels are slim on almost all sides except for the bottom since it houses the Huawei logo at its center. My opinion still hasn’t changed when it comes to these kinds of designs that mostly caters to the youth. It’s one of the ideal phones to get when you are out with friends or just hanging out casually, but I don’t think it’s appropriate to use it for corporate use or any other formal activities that you’ll attend to since it’s too much of an eye-catcher."
    },
    {
        title: "Iphone 11 128GB",
        price: '2450000',
        image01: product_03_image_01,
        image02: product_03_image_02,
        categorySlug: "iphone",
        colors: ["white"],
        slug: "hp-no-03",
        storage: ["128"],
        description: "The Iphone 11 2019 and the Y9 2019, launched last year, have an eerily similar design and structure given that they are both from the Y-series lineup. It has a glossy polycarbonate back panel with a gradient color pattern which still looks very stylish and premium. The bezels are slim on almost all sides except for the bottom since it houses the Huawei logo at its center. My opinion still hasn’t changed when it comes to these kinds of designs that mostly caters to the youth. It’s one of the ideal phones to get when you are out with friends or just hanging out casually, but I don’t think it’s appropriate to use it for corporate use or any other formal activities that you’ll attend to since it’s too much of an eye-catcher."
    },
    {
        title: "Iphone 12 Pro Max 256GB",
        price: '2450000',
        image01: product_04_image_01,
        image02: product_04_image_02,
        categorySlug: "iphone",
        colors: ["white", "blue"],
        slug: "hp-04",
        storage: ["256"],
        description: "The Iphone 12 Pro 2019 and the Y9 2019, launched last year, have an eerily similar design and structure given that they are both from the Y-series lineup. It has a glossy polycarbonate back panel with a gradient color pattern which still looks very stylish and premium. The bezels are slim on almost all sides except for the bottom since it houses the Huawei logo at its center. My opinion still hasn’t changed when it comes to these kinds of designs that mostly caters to the youth. It’s one of the ideal phones to get when you are out with friends or just hanging out casually, but I don’t think it’s appropriate to use it for corporate use or any other formal activities that you’ll attend to since it’s too much of an eye-catcher."
    },
    {
        title: "Iphone 12 Pro Max 256GB",
        price: '2450000',
        image01: product_05_image_01,
        image02: product_05_image_02,
        categorySlug: "iphone",
        colors: ["white"],
        slug: "hp-no-05",
        storage: ["xxl"],
        description: "The Iphone 12 Pro 2019 and the Y9 2019, launched last year, have an eerily similar design and structure given that they are both from the Y-series lineup. It has a glossy polycarbonate back panel with a gradient color pattern which still looks very stylish and premium. The bezels are slim on almost all sides except for the bottom since it houses the Huawei logo at its center. My opinion still hasn’t changed when it comes to these kinds of designs that mostly caters to the youth. It’s one of the ideal phones to get when you are out with friends or just hanging out casually, but I don’t think it’s appropriate to use it for corporate use or any other formal activities that you’ll attend to since it’s too much of an eye-catcher."
    },
    {
        title: "Iphone 12 Pro Max 256GB",
        price: '2450000',
        image01: product_06_image_01,
        image02: product_06_image_02,
        categorySlug: "iphone",
        colors: ["black"],
        slug: "hp-no-06",
        storage: ["32", "64", "256"],
        description: "The Iphone 12 Pro Max launched last year, have an eerily similar design and structure given that they are both from the Y-series lineup. It has a glossy polycarbonate back panel with a gradient color pattern which still looks very stylish and premium. The bezels are slim on almost all sides except for the bottom since it houses the Huawei logo at its center. My opinion still hasn’t changed when it comes to these kinds of designs that mostly caters to the youth. It’s one of the ideal phones to get when you are out with friends or just hanging out casually, but I don’t think it’s appropriate to use it for corporate use or any other formal activities that you’ll attend to since it’s too much of an eye-catcher."
    },
    {
        title: "Samsung Galaxy S22 Ultra 5G 128/256GB",
        price: '199999',
        image01: product_07_image_01,
        image02: product_07_image_02,
        categorySlug: "samsung",
        colors: ["white", "red", "blue"],
        slug: "hp-no-07",
        storage: ["128", "256"],
        description: "The Samsung Galaxy S22 Ultra 5G launched last year, have an eerily similar design and structure given that they are both from the Y-series lineup. It has a glossy polycarbonate back panel with a gradient color pattern which still looks very stylish and premium. The bezels are slim on almost all sides except for the bottom since it houses the Huawei logo at its center. My opinion still hasn’t changed when it comes to these kinds of designs that mostly caters to the youth. It’s one of the ideal phones to get when you are out with friends or just hanging out casually, but I don’t think it’s appropriate to use it for corporate use or any other formal activities that you’ll attend to since it’s too much of an eye-catcher."
    },
    {
        title: "Samsung Galaxy S22 Ultra 5G 32/64/256GB",
        price: '199999',
        image01: product_08_image_01,
        image02: product_08_image_02,
        categorySlug: "samsung",
        colors: ["white", "red", "black"],
        slug: "hp-no-08",
        storage: ["32", "64", "256"],
        description: "The Samsung Galaxy S22 Ultra 5G launched last year, have an eerily similar design and structure given that they are both from the Y-series lineup. It has a glossy polycarbonate back panel with a gradient color pattern which still looks very stylish and premium. The bezels are slim on almost all sides except for the bottom since it houses the Huawei logo at its center. My opinion still hasn’t changed when it comes to these kinds of designs that mostly caters to the youth. It’s one of the ideal phones to get when you are out with friends or just hanging out casually, but I don’t think it’s appropriate to use it for corporate use or any other formal activities that you’ll attend to since it’s too much of an eye-catcher."
    },
    {
        title: "Samsung Galaxy S22 Ultra 5G 64GB",
        price: '199999',
        image01: product_09_image_01,
        image02: product_09_image_02,
        categorySlug: "samsung",
        colors: ["white", "blue"],
        slug: "hp-no-09",
        storage: ["64"],
        description: "The Samsung Galaxy S22 Ultra 5G launched last year, have an eerily similar design and structure given that they are both from the Y-series lineup. It has a glossy polycarbonate back panel with a gradient color pattern which still looks very stylish and premium. The bezels are slim on almost all sides except for the bottom since it houses the Huawei logo at its center. My opinion still hasn’t changed when it comes to these kinds of designs that mostly caters to the youth. It’s one of the ideal phones to get when you are out with friends or just hanging out casually, but I don’t think it’s appropriate to use it for corporate use or any other formal activities that you’ll attend to since it’s too much of an eye-catcher."
    },
    {
        title: "Oppo Reno F1 64GB",
        price: '999000',
        image01: product_10_image_01,
        image02: product_10_image_02,
        categorySlug: "others",
        colors: ["blue", "black"],
        slug: "hp-no-10",
        storage: ["128"],
        description: "The Samsung Galaxy S22 Ultra 5G launched last year, have an eerily similar design and structure given that they are both from the Y-series lineup. It has a glossy polycarbonate back panel with a gradient color pattern which still looks very stylish and premium. The bezels are slim on almost all sides except for the bottom since it houses the Huawei logo at its center. My opinion still hasn’t changed when it comes to these kinds of designs that mostly caters to the youth. It’s one of the ideal phones to get when you are out with friends or just hanging out casually, but I don’t think it’s appropriate to use it for corporate use or any other formal activities that you’ll attend to since it’s too much of an eye-catcher."
    },
    {
        title: "Xiaomi Mi 4i 64GB",
        price: '999000',
        image01: product_11_image_01,
        image02: product_11_image_02,
        categorySlug: "others",
        colors: ["white", "black"],
        slug: "hp-no-11",
        storage: ["64"],
        description: "The Samsung Galaxy S22 Ultra 5G launched last year, have an eerily similar design and structure given that they are both from the Y-series lineup. It has a glossy polycarbonate back panel with a gradient color pattern which still looks very stylish and premium. The bezels are slim on almost all sides except for the bottom since it houses the Huawei logo at its center. My opinion still hasn’t changed when it comes to these kinds of designs that mostly caters to the youth. It’s one of the ideal phones to get when you are out with friends or just hanging out casually, but I don’t think it’s appropriate to use it for corporate use or any other formal activities that you’ll attend to since it’s too much of an eye-catcher."
    },
    {
        title: "Realme GT X 32/64/256GB",
        price: '999000',
        image01: product_12_image_01,
        image02: product_12_image_02,
        categorySlug: "others",
        colors: ["blue"],
        slug: "hp-no-12",
        storage: ["32", "64", "256"],
        description: "The Samsung Galaxy S22 Ultra 5G launched last year, have an eerily similar design and structure given that they are both from the Y-series lineup. It has a glossy polycarbonate back panel with a gradient color pattern which still looks very stylish and premium. The bezels are slim on almost all sides except for the bottom since it houses the Huawei logo at its center. My opinion still hasn’t changed when it comes to these kinds of designs that mostly caters to the youth. It’s one of the ideal phones to get when you are out with friends or just hanging out casually, but I don’t think it’s appropriate to use it for corporate use or any other formal activities that you’ll attend to since it’s too much of an eye-catcher."
    },
    {
        title: "Iphone 13 Pro Max 256GB",
        price: '1890000',
        image01: product_01_image_01,
        image02: product_01_image_02,
        categorySlug: "iphone",
        colors: ["white", "red"],
        slug: "hp-no-13",
        storage: ["256"],
        description: "The Samsung Galaxy S22 Ultra 5G launched last year, have an eerily similar design and structure given that they are both from the Y-series lineup. It has a glossy polycarbonate back panel with a gradient color pattern which still looks very stylish and premium. The bezels are slim on almost all sides except for the bottom since it houses the Huawei logo at its center. My opinion still hasn’t changed when it comes to these kinds of designs that mostly caters to the youth. It’s one of the ideal phones to get when you are out with friends or just hanging out casually, but I don’t think it’s appropriate to use it for corporate use or any other formal activities that you’ll attend to since it’s too much of an eye-catcher."
    },
    {
        title: "Iphone X 64/128GB",
        price: '1590000',
        image01: product_02_image_01,
        image02: product_02_image_02,
        categorySlug: "iphone",
        colors: ["white", "blue"],
        slug: "hp-no-14",
        storage: ["64", "128"],
        description: "The Samsung Galaxy S22 Ultra 5G launched last year, have an eerily similar design and structure given that they are both from the Y-series lineup. It has a glossy polycarbonate back panel with a gradient color pattern which still looks very stylish and premium. The bezels are slim on almost all sides except for the bottom since it houses the Huawei logo at its center. My opinion still hasn’t changed when it comes to these kinds of designs that mostly caters to the youth. It’s one of the ideal phones to get when you are out with friends or just hanging out casually, but I don’t think it’s appropriate to use it for corporate use or any other formal activities that you’ll attend to since it’s too much of an eye-catcher."
    },
    {
        title: "Iphone XR 128GB",
        price: '1900000',
        image01: product_03_image_01,
        image02: product_03_image_02,
        categorySlug: "iphone",
        colors: ["red", "blue"],
        slug: "hp-no-15",
        storage: ["128"],
        description: "The Samsung Galaxy S22 Ultra 5G launched last year, have an eerily similar design and structure given that they are both from the Y-series lineup. It has a glossy polycarbonate back panel with a gradient color pattern which still looks very stylish and premium. The bezels are slim on almost all sides except for the bottom since it houses the Huawei logo at its center. My opinion still hasn’t changed when it comes to these kinds of designs that mostly caters to the youth. It’s one of the ideal phones to get when you are out with friends or just hanging out casually, but I don’t think it’s appropriate to use it for corporate use or any other formal activities that you’ll attend to since it’s too much of an eye-catcher."
    },
    {
        title: "Real me no 16",
        price: '1940000',
        image01: product_08_image_01,
        image02: product_08_image_02,
        categorySlug: "samsung",
        colors: ["blue", "black"],
        slug: "16",
        storage: ["64", "256"],
        description: "The Samsung Galaxy S22 Ultra 5G launched last year, have an eerily similar design and structure given that they are both from the Y-series lineup. It has a glossy polycarbonate back panel with a gradient color pattern which still looks very stylish and premium. The bezels are slim on almost all sides except for the bottom since it houses the Huawei logo at its center. My opinion still hasn’t changed when it comes to these kinds of designs that mostly caters to the youth. It’s one of the ideal phones to get when you are out with friends or just hanging out casually, but I don’t think it’s appropriate to use it for corporate use or any other formal activities that you’ll attend to since it’s too much of an eye-catcher."
    },
    {
        title: "Android 9",
        price: '1940000',
        image01: product_09_image_01,
        image02: product_09_image_02,
        categorySlug: "samsung",
        colors: ["white", "blue"],
        slug: "hp-no-17",
        storage: ["32", "128", "256"],
        description: "The Samsung Galaxy S22 Ultra 5G launched last year, have an eerily similar design and structure given that they are both from the Y-series lineup. It has a glossy polycarbonate back panel with a gradient color pattern which still looks very stylish and premium. The bezels are slim on almost all sides except for the bottom since it houses the Huawei logo at its center. My opinion still hasn’t changed when it comes to these kinds of designs that mostly caters to the youth. It’s one of the ideal phones to get when you are out with friends or just hanging out casually, but I don’t think it’s appropriate to use it for corporate use or any other formal activities that you’ll attend to since it’s too much of an eye-catcher."
    },
    {
        title: "Nokia e91",
        price: '1940000',
        image01: product_10_image_01,
        image02: product_10_image_02,
        categorySlug: "others",
        colors: ["blue", "black"],
        slug: "hp-no-18",
        storage: ["32", "64", "128", "256"],
        description: "The Samsung Galaxy S22 Ultra 5G launched last year, have an eerily similar design and structure given that they are both from the Y-series lineup. It has a glossy polycarbonate back panel with a gradient color pattern which still looks very stylish and premium. The bezels are slim on almost all sides except for the bottom since it houses the Huawei logo at its center. My opinion still hasn’t changed when it comes to these kinds of designs that mostly caters to the youth. It’s one of the ideal phones to get when you are out with friends or just hanging out casually, but I don’t think it’s appropriate to use it for corporate use or any other formal activities that you’ll attend to since it’s too much of an eye-catcher."
    },
    // 18 products
]

const getAllProducts = () => products

const getProducts = (count) => {
    const max = products.length - count
    const min = 0
    const start = Math.floor(Math.random() * (max - min) + min)
    return products.slice(start, start + count)
}

const getProductBySlug = (slug) => products.find(e => e.slug === slug)

const getCartItemsInfo = (cartItems) => {
    let res = []
    if (cartItems.length > 0) {
        cartItems.forEach(e => {
            let product = getProductBySlug(e.slug)
            res.push({
                ...e,
                product: product
            })
        })
    }
    // console.log(res)
    // console.log('sorted')
    // console.log(res.sort((a, b) => a.slug > b.slug ? 1 : (a.slug < b.slug ? -1 : 0)))
    return res.sort((a, b) => a.id > b.id ? 1 : (a.id < b.id ? -1 : 0))
}

const productData = {
    getAllProducts,
    getProducts,
    getProductBySlug,
    getCartItemsInfo
}

export default productData