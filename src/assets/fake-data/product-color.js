const colors = [
    {
        display: "White",
        color: "white"
    },
    {
        display: "Black",
        color: "black"
    },
    {
        display: "Yellow",
        color: "yellow"
    },
    {
        display: "Blue",
        color: "blue"
    }
]

export default colors