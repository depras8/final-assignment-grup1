const storage = [
    {
        display: "32",
        storage: "32"
    },
    {
        display: "64",
        storage: "64"
    },
    {
        display: "128",
        storage: "128"
    },
    {
        display: "256",
        storage: "256"
    },
    {
        display: "512",
        storage: "512"
    },
]

export default storage