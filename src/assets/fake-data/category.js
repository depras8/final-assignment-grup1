const category = [
    {
        display: "Iphone",
        categorySlug: "iphone"
    },
    {
        display: "Samsung",
        categorySlug: "samsung"
    },
    {
        display: "Others",
        categorySlug: "others"
    }
]

export default category