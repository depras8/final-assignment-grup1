const policy = [
    {
        name: "Free ship",
        description: "Free shipping with orders > 1999K",
        icon: "bx bx-shopping-bag"
    },
    {
        name: "Payment COD",
        description: "Payment on delivery (COD)",
        icon: "bx bx-credit-card"
    },
    {
        name: "Customer VIP",
        description: "Offers for customers VIP",
        icon: "bx bx-diamond"
    },
    {
        name: "Warranty support",
        description: "Change and fix things at all stores",
        icon: "bx bx-donate-heart"
    }
]

export default policy